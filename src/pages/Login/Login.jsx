import React, {useState} from 'react'
import './Login.css'
import api from '../../services/Api.js'
import Button from '../../components/Buttons/Button'
import { useHistory } from 'react-router-dom';


function Login(){
    var cpfInput = ""
    var passwordInput = ""
    const [errorMessage,setErrorMessage] = useState("")
    const history = useHistory();

    const handleSubmit = async () => {
        //Corrigir input
        if (cpfInput.length == 11 && passwordInput != ''){
            cpfInput = cpfInput.split("");
            cpfInput.splice(9, 0, "-");
            cpfInput.splice(6, 0, ".");
            cpfInput.splice(3, 0, ".");
            cpfInput = cpfInput.join('')
            let login = Api.login(cpfInput, passwordInput).then((resp) => {
                if(resp){
                    //Redirecionar
                    history.push("/curriculos");
                }else{setErrorMessage("Seu CPF ou senha estão errados.");}
                
            });
        }else{setErrorMessage("Seu CPF ou senha estão errados.");}
    }

    const Api = api("https://induff-back.herokuapp.com");


    return(
        <main className="login">
            <div className='login-info'><h1><span className="orange">id</span>UFF</h1>
                <h2>Bem vindo ao Sistema Acadêmico da Graduação</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris mattis ex vel mauris porta pulvinar. Integer luctus bibendum ex et faucibus. Vivamus sagittis consectetur quam et rhoncus. Cras imperdiet nisi et elementum dictum. Duis nec turpis eleifend, viverra turpis et, egestas felis. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Phasellus quis posuere sem. </p>
            </div>
            <div className='login-entrar'>
                <h2>Acesse o seu idUFF</h2>
                <p id="mensagem-erro">{errorMessage}</p>
                <div className="inputBoxes">
                    <label htmlFor="cpf">Seu CPF (somente números)</label>
                    <input type="text" id="cpf" onChange={(e) => {cpfInput = e.target.value}}/>
                </div>
                <div className="inputBoxes">
                    <label htmlFor="password">Senha do seu idUFF</label>
                    <input type="password" id="password" onChange={(e) => {passwordInput = e.target.value}}/>
                </div>
                <Button func={handleSubmit} text="Logar" link="#"/>
            </div>
        </main>
    );
}

export default Login