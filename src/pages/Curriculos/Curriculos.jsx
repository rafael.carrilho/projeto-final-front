import React, {useState, useEffect} from 'react'
import './Curriculo.css'
import { useHistory } from 'react-router-dom';
import SideMenu from '../../common/SideMenu/SideMenu'
import List from '../../components/List/List'
import Button from '../../components/Buttons/Button';
import Dropdown from '../../components/Buttons/Dropdown';
import api from '../../services/Api';

function Curriculo(){
    function updateList(){
        const Api = api("https://induff-back.herokuapp.com");
        Api.getCourses().then(resp => {
            let dropdownList = []
            let fullList = []
            let cursos = []
            for(let x in resp){
                dropdownList.push(resp[x].name)
                for(let y in resp[x]["seasons"]){
                    let row = []
                    cursos.push(resp[x].name)
                    let materias = resp[x]["seasons"]
                    row.push(materias[y]["subject"]["name"])
                    row.push(materias[y]["season"])
                    row.push(materias[y]["subject"]["total_workload"])
                    console.log(materias[y]["subject"])
                    fullList.push(row)
                }
            }
            setRows(fullList)
            setCourses(cursos)
            setDropdownOptions(dropdownList)
            
        });
    }
    useEffect(() => {updateList()}, [])
    
    // var rows =[["Cálculo","1° Período","60"],["Cálculo","1 Período","60"],["Cálculo","1 Período","60"],["a","b","c",<Button text={"sup"}/>]]
    // var courses; // Pegar courses do back
    var titles; // Predefinido pela página (nesse caso curriculo)
    // var rows; // pegar do back e fazer um [] 3d
    // var gridTemplate; // grid template definido pelo tamanho de chars max em cada coluna ou hardcoded por página
    // pegardaapi(); retorna courses, rows, gridTemplate
    const [courseFilter,setCourseFilter] = useState('')
    const [courses,setCourses] = useState([])
    const [rows,setRows] = useState([])
    const [dropdownOptions,setDropdownOptions] = useState([])

    return(
        <main className="curriculo">
            <SideMenu/>
            <section className="curriculos">
                <section className="curriculos-texts">
                    <h1>Visualizar Curriculos</h1>
                    <p>Visualize aqui o currículo de qualquer um dos cursos da Uff</p>
                    <div className="dropGroup">
                        <p>Cursos: </p>
                        <Dropdown setFilter={setCourseFilter} options={dropdownOptions} defaultOption={''}></Dropdown>
                    </div>
                </section>
                <section className="list">
                    <List courseFilter={courseFilter} titles={["Nome","Período","CHT"]} rows={rows} grid={"2fr 1fr 1fr"} coursePerRow={courses}/>
                </section>
            </section>
        </main>
    );
}

export default Curriculo