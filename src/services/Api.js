import axios from 'axios';


const urls = {
    login: "/auth/login",
    course: "/courses",
    subject: "/subjects",
    classroom: "/classrooms",
    user: "/users"
}

const Api = (uri) => ({
    _axios: axios.create({
        baseURL: uri
    }),
    setToken: async function(value){
        //config in axios and store in AsyncStorage
        this._axios.defaults.headers.common['Authorization'] = value;
        localStorage.setItem('@token', value);
    },
    login: async function (cpf, password){
        let loginFullfiled = false;
        
        await this._axios.post(urls.login, {user: {cpf:cpf, password:password}}).then(async response => {
            //Keeping token
            this.setToken(response.data.token);
            localStorage.setItem('@user.name', response.data.user.account.name);
            localStorage.setItem('@user.registration', response.data.user.account.registration);
            localStorage.setItem('@user.role', response.data.user.title);
            localStorage.setItem('@user.id', response.data.user.account.id);

            loginFullfiled = true;

        }).catch(err => {
            alert("Login error: ");
            console.warn(err);
        });

        return loginFullfiled;
        
    },
    getCourses: async function (setCourses,setRows,setGridTemplate){
        let resp = await this._axios.get(urls.course, {}).then(async resp => {
            return resp.data;
        })

        return resp;
    },
    getSubjects: async function(){
        let resp = await this._axios.get(urls.subject).then(resp => {
            return resp.data;
        })

        return resp;
    },
    getClassrooms: async function(){
        let resp = await this._axios.get(urls.classroom).then(resp => {
            return resp.data;
        })

        return resp;
    },
    //Não sei se é assim (deu permissão negada)
    editUser: async function(userData, userID){
        let editOK = false;
        await this._axios.patch(`${urls.user}/${userID}`, {user:{userData}}).then(resp => {
            //Atualizar as informações do usuario
            localStorage.setItem('@user.name', resp.data.user.account.name);
            localStorage.setItem('@user.registration', resp.data.user.account.registration);

            editOK = true;
            
        }).catch(err => {
            alert("Error editing user info.");
            console.warn(err);
        })

        return editOK;
    },
    createUser: async function(userData){
        let creationOk = false;
        await this._axios.post(urls.user, {user:userData}).then(resp => {
            creationOk = true;
        }).catch(err => {
            alert("Error creating new user");
            console.warn(err);
        })

        return creationOk;
    }


});

export default Api;

