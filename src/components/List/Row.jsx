import React, {useState, useEffect} from 'react'
import './List.css'

function Row(props) {
    // console.log("#### from props")
    // console.log(props.grid)
    // props.grid.push('')
    // console.log(props.grid)
    // let template = props.grid.join('fr ')
    // console.log(template)
    
    // console.log("#### cloning props")
    // console.log(props.grid)
    // var b = []
    // for(let x of props.grid){b.push(x)}
    // let a = b.join('fr ')
    // console.log(a)


    // console.log("#### hardcoded list")
    // let c = ['1','2','3',''].join("fr ")
    // console.log(c)

    const rowDiv = {
        display: 'grid',
        gridTemplateColumns: props.grid
    }
    return(
        <div style={rowDiv} className={"row"}>
            {props.texts.map((text) => (
                <div>{text}</div>
            ))}
        </div>
    )
}

export default Row