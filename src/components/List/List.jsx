import React, {useState, useEffect} from 'react'
import './List.css'
import Row from './Row.jsx'

function List(props) {
    var i = -1
    return(
        <main className="List">
            <Row texts={props.titles} grid={props.grid}/>
            {props.rows.map((row) => {
                i += 1
                if(props.coursePerRow[i] == props.courseFilter){
                    return <Row texts={row} grid={props.grid}/>
                }
            })}
        </main>
    )
}

export default List