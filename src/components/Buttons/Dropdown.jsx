import React, {useState, useEffect} from 'react'
import './Dropdown.css'

function Dropdown(props) {
    const [dStyle, setDStyle] = useState({display: 'none'})
    const [isDisplayed,setIsDisplayed] = useState(false)
    const [selectedItem,setSelectedItem] = useState(props.defaultOption)
    
    function toggleDropdown(displayed){
        if (displayed){
            setDStyle({display:'none'})
        setIsDisplayed(false)
        }else{
            setDStyle({display:'inline'})
        setIsDisplayed(true)
        }
    }
    function selectI(value){
        props.setFilter(value)
        setSelectedItem(value)
    }
    return(
        <div className="dropdown" clickOutside onClick={() => {toggleDropdown(isDisplayed)}}>
            <p>{selectedItem}</p>
            <div className="arrow">
            </div>
            <div className="opts" style={dStyle}>
                {props.options.map((opt) => (
                    <p onClick={(e) => {selectI(e.target.className)}} id="opt" className={opt}>{opt}</p>
                ))}
            </div>
        </div>
    )
}

export default Dropdown