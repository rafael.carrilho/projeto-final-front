import React, {useState, useEffect} from 'react'
import './Button.css'
import {Link} from 'react-router-dom'

function Button(props) {
    return(
        <Link onClick={props.func} className="Button" to={props.link}>{props.text}</Link>
    )
}

export default Button