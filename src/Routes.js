import React from 'react'
import {BrowserRouter, Switch, Route} from 'react-router-dom'
import Login from './pages/Login/Login.jsx'
import Curriculos from './pages/Curriculos/Curriculos.jsx'

function Routes(){
    return(
        <BrowserRouter>
            <Switch>
                <Route exact path='/'>
                    <Login/>
                </Route>

                <Route exact path="/curriculos">
                    <Curriculos/>
                </Route>

                {/* <Route path='/'>
                    404
                </Route> */}
            </Switch>
        </BrowserRouter>
    );   
}

export default Routes
