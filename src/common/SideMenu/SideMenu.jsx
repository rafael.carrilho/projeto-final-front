import React, {useState} from 'react'
import './SideMenu.css'
import Button from '../../components/Buttons/Button'
import { Link } from 'react-router-dom'


function SideMenu(props) {
    
    // const [userName,setUserName] = useState("")
    // const [role,setRole] = useState("")
    // const [registration,setRegistration] = useState("")
    // userInfo([setUserName,setRole,setRegistration],)
    var optionsList = [
        {
            text:"Histórico",
            link:"#"
        },
        {
            text:"Curriculo",
            link:"/curriculos"
        },
        {
            text:"Sair",
            link:"/"
        }
    ]
    const [optState,setOptState] = useState(optionsList)
    
    const [show,setShow] = useState(false)
    const [showIdState,setShowIdState] = useState("hidden")
    function toggleSide(){
        if (show){
            setShowIdState("shown")
            setShow(false)
        }else{
            setShowIdState("hidden")
            setShow(true)
        }
    }
    return (
        <>
            <button className="hide-show-menu" onClick={() => {toggleSide()}}></button>
            <header id={showIdState} className="menu">
                <section className="Info">  
                    {["@user.name","@user.role","@user.registration"].map((stored) => (
                        <p>{localStorage.getItem(stored)}</p>
                    ))}          
                    <Button text="Editar dados" link="#"/>
                </section>
                <section className="Options">
                    {optState.map((item) => {
                        let id = ''
                        if(item.link === window.location.pathname){ id = "selected"}
                        return <div> <Link to={item.link}>{item.text}<div id={id}></div></Link> </div>
                    })}
                </section>
            </header>
        </>
    )
}

export default SideMenu
